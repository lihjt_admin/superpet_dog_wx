import { Router } from "../../utils/common.js";
import { UserStorage } from "../../utils/storageSevice.js";
import { CACHE } from "../../utils/cache.js";

Page({

  data: {
    userInfo: {},
    petCount: 0,
    hasPetCard: false
  },

  onLoad: function (options) {
    let userInfo = UserStorage.getData()
    if (userInfo) {
      let petCount = "还没有汪牌哦，点击创建"
      let hasPetCard = false
      if (CACHE.userPetCount>0){
        petCount = "您已拥有" + CACHE.userPetCount+"张汪牌"
        hasPetCard = true
      }
      this.setData({
        userInfo: userInfo,
        petCount: petCount,
        hasPetCard: hasPetCard
      })
    }
  },
  navToFeedList: function () {
    Router.navigateTo("../feedlist/feedlist");
  },
  navToDogFriend: function () {
    Router.navigateTo("../dogfriend/dogfriend");
  },
  navToDogCard: function () {
    Router.navigateTo("../greeting-card-add/greeting-card-add");
  },
  navToAbout: function () {
    Router.navigateTo("../aboutwang/aboutwang");
  },
  navToIndex: function () {
    wx.switchTab({
      url: '../index/index'
    })
  }
})